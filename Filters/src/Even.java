import java.util.ArrayList;
import java.util.List;

public class Even implements Filterable {

	@Override
	public List<Integer> filter(List<Integer> numbers) {
		List<Integer> filter = new ArrayList<Integer>();
		for (Integer n : numbers) {
	if (n %2 == 0) {
		filter.add(n);
	}
}
		return filter;

	}

}
