package com.imgn.prime;
import java.util.*;
public interface Filterable {
public List<Integer> filter(List<Integer> numbers);

}
