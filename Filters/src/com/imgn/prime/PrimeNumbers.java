package com.imgn.prime;

import java.util.*;

public class PrimeNumbers {
	public static List<Integer> filterPrime(List<Integer> numbers) {
		List<Integer> filtered = new ArrayList<Integer>();
		for (Integer n : numbers) {
boolean prime = true;
for (Integer i = n - 1; i > 1; i--) {
	if (n % i == 0) {
		prime = false;
break;
	}
}
if (prime) {
	filtered.add(n);
	}
}
		return filtered;
	}

	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(1);
		numbers.add(2);
		numbers.add(3);
		numbers.add(4);
		numbers.add(5);
		numbers.add(6);
		numbers.add(8);
		numbers.add(9);
		numbers.add(10);
		numbers.add(11);
		numbers.add(12);
		numbers.add(13);
		numbers.add(14);
		numbers.add(15);
		numbers.add(16);
		numbers.add(17);
		numbers.add(18);
		numbers.add(19);
		numbers.add(20);
		List<Integer> filtered = filterPrime(numbers);

		System.out.println(filtered);
	}
}
