package prac2;

public class Truck extends Vehicle {

	@Override
	protected Integer getDistance() {
		return 5;
	}

	@Override
	public String getName() {
		return "Truck";
	}

}
