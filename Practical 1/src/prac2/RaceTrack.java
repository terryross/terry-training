package prac2;
import java.util.ArrayList;
import java.util.List;

public class RaceTrack {
	private List<Vehicle> vehicles = new ArrayList<Vehicle>();
	private Integer raceDistance;

public RaceTrack(Integer raceDistance) {
	this.raceDistance = raceDistance;
}

public void addToRace(Vehicle vehicle) {
		vehicles.add(vehicle);
	}

	public void startRace() {
		boolean raceComplete = false;
		while (!raceComplete) {
			for (Vehicle v : vehicles) {
				v.drive();

				if (v.getDistanceCovered() >= raceDistance) {
					System.out.println(v.getName() + " is the winner!");
					raceComplete = true;
					break;
				}
				}
			}
		}

}
