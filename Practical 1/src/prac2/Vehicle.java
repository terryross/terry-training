package prac2;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public abstract class Vehicle {
    private Integer distanceCovered = 0;

    protected abstract Integer getDistance();

    public abstract String getName();

    public void drive() {
        distanceCovered = distanceCovered + getDistance();

        try {
            FileWriter fw = new FileWriter("out.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(getName() + " has covered " + distanceCovered + "\n");
            System.out.println(getName() + " has covered " + distanceCovered);

            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Integer getDistanceCovered() {
        return distanceCovered;
    }
}
