package prac2;

public class Car extends Vehicle {

	@Override
	protected Integer getDistance() {
		return 15;
	}

	@Override
	public String getName() {
		return "Car";
	}

}
