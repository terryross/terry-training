package prac2;

public class Bike extends Vehicle {

	@Override
	protected Integer getDistance() {
		return 20;
	}

	@Override
	public String getName() {
		return "Bike";
	}

}
