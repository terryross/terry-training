package prac2;
import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		RaceTrack raceTrack = new RaceTrack(500);
		raceTrack.addToRace(new Car());
		raceTrack.addToRace(new Truck());
		raceTrack.addToRace(new Bicycle());
		raceTrack.addToRace(new Bike());

		raceTrack.startRace();
		
	}
}
