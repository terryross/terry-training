package prac1;

import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		RaceTrack raceTrack = new RaceTrack(200);
		raceTrack.addToRace(new Car());
		raceTrack.addToRace(new Truck());
		raceTrack.addToRace(new Bicycle());
		raceTrack.addToRace(new Bike());
		
		
		raceTrack.startRace();
	}
}
