package prac1;

public class Car extends Vehicle {

	@Override
	protected Integer getDistence() {
		return 10;
	}
		
	@Override
	public String getName() {
		return "Car";
	}

}