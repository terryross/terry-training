package prac1;

public class Bicycle extends Vehicle {

	@Override
	protected Integer getDistence() {
		return 1;
	}

	@Override
	public String getName() {
		return "Bicycle";
	}

}
