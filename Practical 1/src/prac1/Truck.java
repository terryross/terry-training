package prac1;

public class Truck extends Vehicle {

	@Override
	protected Integer getDistence() {
		return 5;
	}
	
	@Override
	public String getName() {
		return "Truck";
	}

}
