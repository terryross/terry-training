package prac1;

import java.util.ArrayList;
import java.util.List;

public class RaceTrack {
	private List<Vehicle> vehicles = new ArrayList<Vehicle>();
	private Integer raceDistence;
	
	public RaceTrack(Integer raceDistence) {
		this.raceDistence = raceDistence;
	}
	
	public void addToRace(Vehicle vehicle) {
		vehicles.add(vehicle);
	}
	
	public void startRace() {
		boolean raceComplete = false;
		while(!raceComplete) {
			for(Vehicle v : vehicles) {
				v.drive();
				
				if (v.getDistenceCovered() >= raceDistence) {
					//we have a winner
					System.out.println(v.getName() + " is the winner!!!");
					raceComplete = true;
					break;
				}
			}
		}
	}

}