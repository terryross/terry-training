package prac1;

public class Bike extends Vehicle {

	@Override
	protected Integer getDistence() {
		return 20;
	}

	@Override
	public String getName() {
		return "Bike";
	}

}
