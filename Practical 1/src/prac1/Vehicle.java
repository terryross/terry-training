package prac1;

public abstract class Vehicle {
	private Integer distenceCovered = 0;
	
	protected abstract Integer getDistence();
	public abstract String getName();
	
	public void drive() {
		distenceCovered = distenceCovered + getDistence();
		System.out.println(getName() + " has covered " + distenceCovered);
	}

	public Integer getDistenceCovered() {
		return distenceCovered;
	}
}