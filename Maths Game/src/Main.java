import java.util.*;

public class Main {


	public static void main(String[] args) {
		System.out.println("Hello!");

		boolean bool = true;
		while (bool == true) {
			System.out.println("Would you like to play? \n Please enter yes or no.");
			Scanner scanner = new Scanner(System.in);
			String answer = scanner.nextLine();
			if (answer.equals("yes") || answer.equals("Yes")) {
				bool = true;
System.out.println("Great, then let's begin! If, at any time you want to quit the game, just type Q, and you'll be taken back to the main menu.");
				MainMenu.getOptions();
			} else if (answer.equals("no") || answer.equals("no")) {
				bool = false;
				System.exit(0);
			} else {
				System.out.println("Invalid command");
MainMenu.getOptions();

			}

		}
	}

}
