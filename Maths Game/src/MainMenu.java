import java.util.Scanner;

public class MainMenu {
	public static void getOptions() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please choose an option: \n Press 1 to play guess my number! \n Press 2 to test your calculation skills");
		String gmn = scanner.nextLine();
		if (gmn.equals("1")) {
			ChooseGuessingGame.chooseGuessingGame();

		} else if (gmn.equals("2")) {
			CalcDiff.difficultyLevel();
		}
	}


}
