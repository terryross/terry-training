import java.util.Scanner;

public class ChooseGuessingGame {
	public static void chooseGuessingGame() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Press 1 to guess between 1 and 10 \n press two to choose between 1 and 100");
		String option = scanner.nextLine();
		if (option.equals("1")) {
			Guess1.guessingGame1();

		} else if (option.equals("2")) {
			Guess2.guessingGame2();
		} else {
			System.out.println("Invalid command.");
			MainMenu.getOptions();
		}
	}


}
