import java.util.Scanner;

public class CalcGame1 {
	public static void calculatingGame1() {
		boolean bool = true;
		while (bool == true) {
			Scanner scanner = new Scanner(System.in);

			int o = (int) Generate.getRandomInt(1, 10);
			int p = (int) Generate.getRandomInt(1, 10);
			int q = o + p;
			System.out.println("What do you get if you add" + o + " and " + p + "?");
			String answer = scanner.nextLine();
			Integer ans = Integer.parseInt(answer);
			if (ans.equals(q)) {
				bool = true;
				System.out.println(q + " is correct!");
			} else {
				bool = false;
				System.out.println("Invalid command");
			}
		}
	}
}
