package com.mysql;

import java.sql.*;

public abstract class SQLCommand {

	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/Test";
	static final String USER = "root";
	static final String PASS = "drizzle1";
	protected Connection conn = null;
	protected Statement st = null;

	public void execute(String sql) {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			st = conn.createStatement();
		executeImpl(sql);			
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}

		}
	}

	public abstract void executeImpl(String sql) throws Exception;
}
