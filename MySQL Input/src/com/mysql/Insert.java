package com.mysql;

import java.sql.*;

import java.util.*;

public class Insert {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/Test";
	static final String USER = "root";
	static final String PASS = "drizzle1";

	public String SQL;

	public static String query;

	public static void getResults() {
		try {
			query = "SELECT * FROM employees";

			Retrieve retrieve = new Retrieve();
			retrieve.execute(query);
			List<Person> people = retrieve.getPeople();
			
			for(Person p : people) {
				String first = p.getFirstName();
				String last = p.getSurname();
				String id = p.getIdNumber();

				System.out.print(first + ", ");
				System.out.print(last + ", ");
				System.out.println(id);

			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		boolean bool = true;
		while (bool == true) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Do you want to enter a new record?");
			String input = scanner.nextLine();
			if (input.equals("yes") || input.equals("Yes")) {
				bool = true;
				System.out.println("Please enter employee's first name");
				String firstName = scanner.nextLine();
				System.out.println("Please enter employee's surname");
				String surname = scanner.nextLine();
				System.out.println("Please enter employee's ID");
				String IDNumber = scanner.nextLine();
				int id = Integer.parseInt(IDNumber);

				UpdateCommand updateCommand = new UpdateCommand();
				updateCommand.execute(
						"INSERT INTO employees " + "VALUES ('" + firstName + "','" + surname + "'," + IDNumber + ")");
				getResults();
			} else if (input.equals("no") || input.equals("No")) {
				bool = false;
				System.exit(0);

			} else {
				System.out.println("Invalid command");
			}
		}
	}
}