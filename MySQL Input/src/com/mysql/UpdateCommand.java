package com.mysql;

public class UpdateCommand extends SQLCommand {

	@Override
	public void executeImpl(String sql) throws Exception {
		st.executeUpdate(sql);
	}

}
