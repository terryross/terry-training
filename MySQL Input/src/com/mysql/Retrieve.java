package com.mysql;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Retrieve extends SQLCommand {
	private ResultSet rs;
	private List<Person> people = new ArrayList<>();
	
	@Override
	public void executeImpl(String sql) throws Exception {
		Statement st = conn.createStatement();
		rs = st.executeQuery(sql);
		
		while (rs.next()) {
			String first = rs.getString("firstName");
			String last = rs.getString("surname");
			String id = rs.getString("IDNumber");
			
			Person p = new Person();
			p.setFirstName(first);
			p.setSurname(last);
			p.setIdNumber(id);
			
			people.add(p);
		}
	}

	public List<Person> getPeople() {
		return people;
	}

}
