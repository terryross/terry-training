package hello;

import java.util.ArrayList;
import java.util.List;

public class WordSimilarities {
	public static List<String> findCommonWords(List<String> wordsA, List<String> wordsB) {
		List<String> commonWords = new ArrayList<String>();

		for (String wa : wordsA) {
			for (String wb : wordsB) {
				if (wa.equals(wb)) {
					commonWords.add(wb);
				}
			}
		}
		return commonWords;
	}

	public static void main(String[] args) {

		List<String> wordsA = new ArrayList<String>();
		wordsA.add("This");
		wordsA.add("is");
		wordsA.add("a");
		wordsA.add("test");
		List<String> wordsB = new ArrayList<String>();
		wordsB.add("This");
		wordsB.add("is");
		wordsB.add("something");
		wordsB.add("else");
List<String> commonWords = findCommonWords(wordsA, wordsB);

		System.out.println(commonWords);
	}
}
