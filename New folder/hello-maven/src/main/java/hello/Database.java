package hello;


public class Database {
	private String firstName;
	private String surname;
private String company;
	private String position;
	public Database() {

	}

	public Database(String firstName, String surname, String company, String position) {
		this.firstName = firstName;
		this.surname = surname;
this.company = company;
		this.position = position;

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
public String getCompany() {
	return company;
}
public void setCompany(String company) {
	this.company = company;
}
public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

}