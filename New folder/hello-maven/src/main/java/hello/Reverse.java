package hello;
import com.google.common.base.Preconditions;

public class Reverse {
	public static String reverseWords(String reverse) {
		String[] words = reverse.split("\\s+");
		String out = "";
		for (int i = words.length - 1; i >= 0; i--) {

			out = out + " " + words[i];
		}
Preconditions.checkNotNull(out);

		System.out.println(out);
		return out;

	}

	public static void main(String[] args) {

		String s = "One fine day in the middle of the night";
		reverseWords(s);
		String t = "I did it";
		reverseWords(t);
	}

}