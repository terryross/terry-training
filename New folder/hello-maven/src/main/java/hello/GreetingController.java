package hello;

import com.google.common.base.Preconditions;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
public class GreetingController {

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@RequestMapping(value = "/greeting", method = { RequestMethod.GET })
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}

	@RequestMapping(value = "/greeting", method = { RequestMethod.POST })
	public Greeting greeting(@RequestBody Greeting greeting) {
		Preconditions.checkNotNull(greeting.getId());
		return greeting;

	}

}
