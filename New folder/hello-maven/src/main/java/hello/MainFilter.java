package hello;

import java.util.ArrayList;
import java.util.List;

public class MainFilter {

	public static List<Integer> filtered(List<Integer> numbers, Filterable filterable) {
		return filterable.filter(numbers);
	}

	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(1);
		numbers.add(1);
		numbers.add(2);
		numbers.add(2);
		numbers.add(3);
		numbers.add(4);
		numbers.add(5);
		numbers.add(6);
		numbers.add(7);
		OddFilter oddNumbers = new OddFilter();
		List<Integer> oddFilter = filtered(numbers, oddNumbers);
		System.out.println("Odd numbers");
		System.out.println(oddFilter);

		EvenFilter evenNumbers = new EvenFilter();
		List<Integer> evenFilter = filtered(numbers, evenNumbers);
		System.out.println("Even numbers");
		System.out.println(evenFilter);
	}
}
