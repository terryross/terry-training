package hello;

import java.util.ArrayList;
import java.util.List;

public class RecurringNumber {
	public static Integer countRecurringNumber(List<Integer> numbers, Integer recurringNumber) {
Integer counter = 0;
		for (Integer n : numbers) {
					if (n.equals(recurringNumber)) {
counter ++;
		}
		}
		return counter;
	}

	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(2);
		numbers.add(3);
		numbers.add(4);
		numbers.add(2);
		numbers.add(6);
		numbers.add(2);
		numbers.add(3);
Integer counter = countRecurringNumber(numbers, 3);
System.out.println(counter);

	}
}
