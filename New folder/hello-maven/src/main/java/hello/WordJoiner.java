package hello;

import java.util.ArrayList;
import java.util.List;

public class WordJoiner {
	public static String createSentence(List<String> words) {
		String sentence = "";
		for (String w : words) {
			sentence = sentence + w + " ";
		}
		return sentence;
	}

	public static void main(String[] args) {
		List<String> words = new ArrayList<String>();
		words.add("This");
		words.add("is");
		words.add("boring");
String sentence = createSentence(words);
System.out.println(sentence);
	}

}
