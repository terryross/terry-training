package hello;

import java.util.ArrayList;
import java.util.List;

public class SumOfNumbers {
	public static Integer addNumbers(List<Integer> numbers) {
		Integer sum = 0;
		for (Integer n : numbers) {
			sum += n;
		}
		System.out.println(sum);
		return sum;
	}

	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(2);
		numbers.add(5);
		numbers.add(55);
		addNumbers(numbers);
	}
}