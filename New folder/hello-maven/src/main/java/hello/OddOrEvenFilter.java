package hello;

import java.util.*;

public class OddOrEvenFilter {
	public List<Integer> numbers = new ArrayList<Integer>();
	public Boolean odd;
public OddOrEvenFilter() {

	}
public OddOrEvenFilter(List<Integer> numbers, Boolean odd) {
this.numbers = numbers;
	this.odd = odd;
}
	public List<Integer> getNumbers() {
		return numbers;
	}
	public void setNumbers(List<Integer> numbers) {
		this.numbers = numbers;
	}
	public Boolean getOdd() {
		return odd;
	}
	public void setOdd(boolean odd) {
		this.odd = odd;
	}
}
