package hello;

import java.util.List;

public interface Filterable {
	public List<Integer> filter(List<Integer> numbers);

}
