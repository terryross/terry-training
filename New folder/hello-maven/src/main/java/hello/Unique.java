package hello;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Unique {
	public static Map<Integer, Integer> countUnique(List<Integer> numbers) {
		Map<Integer, Integer> values = new HashMap<Integer, Integer>();

		for (Integer n : numbers) {
		Integer count = values.get(n);

			if (count == null) {
			values.put(n,1);

	} else {
				count = count + 1;
				values.put(n, count);
			}
		}
		return values;
	}

	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(1);
				numbers.add(2);
		numbers.add(3);
		numbers.add(2);
		numbers.add(4);
		numbers.add(1);
Map<Integer, Integer> values = countUnique(numbers);
		System.out.println(values);
	}
}
