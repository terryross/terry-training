package hello;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EvenNumbers {
	public static Set<Integer> displayEvenNumbers(List<Integer> numbers) {
		Set<Integer> values = new HashSet<Integer>();
		for (Integer n : numbers) {
if (n % 2 == 0) {
values.add(n);	
}
		}
return values;
	}

public static void main(String[] args) {
	List<Integer> numbers = new ArrayList<Integer>();
numbers.add(1);
numbers.add(2);
numbers.add(2);
numbers.add(3);
numbers.add(4);
numbers.add(4);
numbers.add(6);
 Set<Integer> values = displayEvenNumbers(numbers);
System.out.println(values);

}
}
