package hello;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mongodb.*;

@RestController
public class DatabaseController {
	private static MongoClient mongoClient;
	private static DB db;
	private static DBCollection people;

	public void getMongoClient() throws Exception {
		mongoClient = new MongoClient("localhost", 27017);
	}

	public void getDatabase() throws Exception {
		db = mongoClient.getDB("employees");
	}

	public void getCollection() {
		people = db.getCollection("people");
	}

	public void getPeople(Database persons) throws Exception {
		getMongoClient();
		getDatabase();
		getCollection();
		DBObject person1 = new BasicDBObject();
		person1.put("firstName", persons.getFirstName());
		person1.put("surname", persons.getSurname());
		person1.put("company", persons.getCompany());
		person1.put("position", persons.getPosition());

		people.insert(person1);

	}

	@RequestMapping(value = "/mongo", method = { RequestMethod.POST })
	public Database database(@RequestBody Database persons) throws Exception {
		getPeople(persons);
		return persons;
	}

	@RequestMapping(value = "/mongo", method = { RequestMethod.GET })
	public Database data(Database persons) throws Exception {
		getMongoClient();
		getDatabase();
		getPeople(persons);
		return persons;

	}
}
