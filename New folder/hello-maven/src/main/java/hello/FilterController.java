package hello;

import java.net.UnknownHostException;
import java.util.ArrayList;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.*;

@RestController
public class FilterController {
	@RequestMapping(value = "/filter", method = { RequestMethod.GET })
public List<Integer> numbers() {
List<Integer> numbers = new ArrayList<Integer>();
numbers.add(1);
numbers.add(2);
		return numbers;
	}
	@RequestMapping(value = "/filter", method = { RequestMethod.POST })
	public OddOrEvenFilter filter(@RequestBody OddOrEvenFilter filtered) {

		if (filtered.odd) {
OddFilter oddFilter = new OddFilter();
filtered.setNumbers(oddFilter.filter(filtered.numbers));
filtered.numbers = oddFilter.filter(filtered.numbers);
		} else {
		EvenFilter evenFilter = new EvenFilter();
		filtered.setNumbers(evenFilter.filter(filtered.numbers));
		filtered.numbers = evenFilter.filter(filtered.numbers);
		}

return filtered;
	}
	@RequestMapping(value = "/sum", method = { RequestMethod.POST })
public Integer addNumbers(@RequestBody Sum sum) {
Integer add = 0;
for (Integer n : sum.getNumbers()) {
	add += n;
}
	
return add;	
	}
	@RequestMapping(value = "/hello{name}", method = { RequestMethod.GET })
public String name(@RequestParam(value = "name") String name) {
	return name;	
	}

}

